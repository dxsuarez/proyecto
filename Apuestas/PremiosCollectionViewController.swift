//
//  PremiosCollectionViewController.swift
//  proyecto
//
//  Created by Desarrollo on 8/11/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class PremiosCollectionViewController: UICollectionViewController {

    var data = Data()
    var premios: [Premio] = []
    var premio: Premio?
    //var premioId = 22
    
    /*
    var selectedPriority:String? {
        didSet {
            if let priority = selectedPriority {
                selectedPriorityIndex = self.premios.index(where: {$0.id == premio?.id})
            }
        }
    }
 
    var selectedPriorityIndex:Int?
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        premios = data.getPremios()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return premios.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let premio = premios[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PremiosCell", for: indexPath) as! PremioCollectionViewCell
        cell.premioImageView!.image = UIImage(named: "p-\(premio.id)")!
        
        return cell
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        premio = premios[indexPath.row]
        //premioId = (premio?.id)!
        //selectedPriority = premios[indexPath.row].nombre
        //print(premio?.nombre)
        performSegue(withIdentifier: "unwindFromPremios", sender: self)
        //navigationController?.popViewController(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }

}

import UIKit
import Alamofire
import AlamofireObjectMapper
class FaseGruposViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK:-Variables Globales
    var jsonArray: NSArray?
    var nombreEquipos: Array<String> = []
    var posicionEquipos: Array<String> = []
    var puntosEquipos: Array<String> = []
    var grupoEquipos: Array<String> = []
    
    

    //MARK:-Outlet
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:-Load
    override func viewDidLoad() {
        super.viewDidLoad()
        llenadoTablaA()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaB()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaC()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaD()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaE()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaF()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaG()
        tableView.reloadData()
        usleep(2000)
        llenadoTablaH()
        tableView.reloadData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK:-Action
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nombreEquipos.count;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    
    func llenadoTablaA(){
    
            Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1804&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
                if let JSON = response.result.value{
                    self.jsonArray = JSON as? NSArray
                    for item in self.jsonArray! as! [NSDictionary]{
                        
                        let name = item["team_name"] as? String
                        let posicion = item["overall_league_position"] as? String
                        let puntos = item["overall_league_PTS"] as? String
                        let grupo = item["league_name"] as? String
                        self.nombreEquipos.append((name)!)
                        self.posicionEquipos.append((posicion)!)
                        self.puntosEquipos.append((puntos)!)
                        self.grupoEquipos.append((grupo)!)
                    }
                    self.tableView.reloadData()
                }
            }
    }
    
    func llenadoTablaB(){
        
        
        
        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1805&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func llenadoTablaC(){

        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1806&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func llenadoTablaD(){

        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1807&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func llenadoTablaE(){

        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1808&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func llenadoTablaF(){

        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1809&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    func llenadoTablaG(){

        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1810&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    func llenadoTablaH(){

        Alamofire.request("https://apifootball.com/api/?action=get_standings&league_id=1811&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.result.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{

                    let name = item["team_name"] as? String
                    let posicion = item["overall_league_position"] as? String
                    let puntos = item["overall_league_PTS"] as? String
                    let grupo = item["league_name"] as? String
                    self.nombreEquipos.append((name)!)
                    self.posicionEquipos.append((posicion)!)
                    self.puntosEquipos.append((puntos)!)
                    self.grupoEquipos.append((grupo)!)
                }
                self.tableView.reloadData()
            }
        }
        
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "CellGrupos", for: indexPath) as! GruposViewCell
        cell1.posLabel.text = self.posicionEquipos[indexPath.row]
        cell1.posNombre.text = self.nombreEquipos[indexPath.row]
        cell1.puntosLabel.text = self.puntosEquipos[indexPath.row]
        cell1.grupoLabel.text = self.grupoEquipos[indexPath.row]
        return cell1
        
        
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        default :
            return "Posición  Nombre   Puntos          Grupo"
        }
    }
    
}

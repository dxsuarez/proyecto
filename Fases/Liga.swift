//
//  Liga.swift
//  Examen1BUEFAChampionsAPP
//
//  Created by Robert on 13/12/17.
//  Copyright © 2017 Robert_Danilo. All rights reserved.
//

import Foundation
import ObjectMapper

class Liga: Mappable {
    var id: Int?
    var nombre: String?
    var region: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        nombre <- map["name"]
        region <- map["region"]
    }
}

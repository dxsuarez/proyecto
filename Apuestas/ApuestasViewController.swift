//
//  ApuestasViewController.swift
//  proyecto
//
//  Created by Diego Suárez on 31/7/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class ApuestasViewController: UIViewController {

    @IBOutlet weak var testLabel: UILabel!
    
    //var data = Data()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        testLabel.text = data.grupos[0].equipos![0].nombre
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

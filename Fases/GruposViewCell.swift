
import UIKit

class GruposViewCell: UITableViewCell {

    
    //MARK:-OUTLET
    
    @IBOutlet weak var posLabel: UILabel!
    @IBOutlet weak var puntosLabel: UILabel!
    
    @IBOutlet weak var grupoLabel: UILabel!
    @IBOutlet weak var posNombre: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

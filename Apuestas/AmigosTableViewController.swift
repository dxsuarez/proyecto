//
//  AmigosTableViewController.swift
//  proyecto
//
//  Created by Desarrollo on 8/11/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class AmigosTableViewController: UITableViewController {

    var data = Data()
    var amigos: [Usuario] = []
    var apostadores: [Usuario]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amigos = data.gruposAmigos[0].amigos
apostadores = []
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return amigos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        let amigo = amigos[indexPath.row]
        cell.textLabel?.text  = "\(amigo.nombre)"
        cell.imageView!.image = UIImage(named: "u-\(amigo.id)")!
        cell.detailTextLabel?.text = "este es el subtitulo"
        cell.accessoryType = .none
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let amigo = amigos[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)
        
        let indice = apostadores!.index(where: {$0.id == amigo.id})
        if(indice != nil) {
            apostadores!.remove(at: indice!)
            cell?.accessoryType = .none
        } else {
            apostadores!.append(amigo)
            cell?.accessoryType = .checkmark
        }
        
        //performSegue(withIdentifier: "toResultadoSegue", sender: self)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    @IBAction func aceptarButtonPreseed(_ sender: Any) {
        performSegue(withIdentifier: "unwindFromAmigos", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    /*
    override func viewWillDisappear(_ animated: Bool) {
        performSegue(withIdentifier: "unwindFromAmigos", sender: self)
        //super.viewWillDisappear(animated)
        
    }
*/
}

//
//  OctavosTableViewCell.swift
//  proyecto
//
//  Created by Wilson Gabriel Ramos Bravo on 8/11/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class OctavosTableViewCell: UITableViewCell {
    @IBOutlet weak var equipoLocalLabel: UILabel!
    
    @IBOutlet weak var equipoVisitanteLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

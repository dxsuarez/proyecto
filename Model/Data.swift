let data = Data()

class Data {
    var grupos: [Grupo]
    var equipos: [Equipo]
    var partidos: [Partido]
    var usuarios: [Usuario]
    var apuestas: [Apuesta]
    var resultados: [Resultado]
    var premios: [Premio]
    var gruposAmigos: [GrupoAmigos]

    init() {
        self.usuarios = [
            Usuario(id: 1, nombre: "Wilson Ramos", grupoAmigos: nil),
            Usuario(id: 2, nombre: "Hugo Montero", grupoAmigos: nil),
            Usuario(id: 3, nombre: "Diego Suàrez", grupoAmigos: nil)
        ]
        
        self.gruposAmigos = [
            GrupoAmigos(id: 1, nombre: "Proyecto iOS", amigos: [])
        ]
        
        self.gruposAmigos[0].amigos = [
            self.usuarios[0],
            self.usuarios[1],
            self.usuarios[2]
        ]
        
        self.apuestas = []
        self.resultados = []
        
        self.premios = [
            Premio(id: 1, nombre: "Pizza"),
            Premio(id: 2, nombre: "Hamburguesa"),
            Premio(id: 3, nombre: "Papas"),
            Premio(id: 4, nombre: "Whisky"),
            Premio(id: 5, nombre: "Vodka"),
            Premio(id: 6, nombre: "Ron"),
            Premio(id: 7, nombre: "Cerveza"),
            Premio(id: 8, nombre: "Entradas estadio")
        ]
        
        self.grupos = [
            Grupo(id: 1, orden: 1, nombre: "Grupo A", equipos: []),
            Grupo(id: 2, orden: 2, nombre: "Grupo B", equipos: []),
            Grupo(id: 3, orden: 3, nombre: "Grupo C", equipos: []),
            Grupo(id: 4, orden: 4, nombre: "Grupo D", equipos: []),
            Grupo(id: 5, orden: 5, nombre: "Grupo E", equipos: []),
            Grupo(id: 6, orden: 6, nombre: "Grupo F", equipos: []),
            Grupo(id: 7, orden: 7, nombre: "Grupo G", equipos: []),
            Grupo(id: 8, orden: 8, nombre: "Grupo H", equipos: [])
        ]
        self.equipos = [
            //Grupo A
            Equipo(id: 11, orden: 1, nombre: "Rusia", grupo: self.grupos[0]),
            Equipo(id: 12, orden: 2, nombre: "Arabia Saudita", grupo: self.grupos[0]),
            Equipo(id: 13, orden: 3, nombre: "Egipto", grupo: self.grupos[0]),
            Equipo(id: 14, orden: 4, nombre: "Uruguay", grupo: self.grupos[0]),
            //Grupo B
            Equipo(id: 21, orden: 1, nombre: "Portugal", grupo: self.grupos[1]),
            Equipo(id: 22, orden: 2, nombre: "España", grupo: self.grupos[1]),
            Equipo(id: 23, orden: 3, nombre: "Marruecos", grupo: self.grupos[1]),
            Equipo(id: 24, orden: 4, nombre: "Irán", grupo: self.grupos[1]),
            //Grupo C
            Equipo(id: 31, orden: 1, nombre: "Francia", grupo: self.grupos[2]),
            Equipo(id: 32, orden: 2, nombre: "Australia", grupo: self.grupos[2]),
            Equipo(id: 33, orden: 3, nombre: "Perú", grupo: self.grupos[2]),
            Equipo(id: 34, orden: 4, nombre: "Dinamarca", grupo: self.grupos[2]),
            //Grupo D
            Equipo(id: 41, orden: 1, nombre: "Argentina", grupo: self.grupos[3]),
            Equipo(id: 42, orden: 2, nombre: "Islandia", grupo: self.grupos[3]),
            Equipo(id: 43, orden: 3, nombre: "Croacia", grupo: self.grupos[3]),
            Equipo(id: 44, orden: 4, nombre: "Nigeria", grupo: self.grupos[3]),
            //Grupo E
            Equipo(id: 51, orden: 1, nombre: "Brasil", grupo: self.grupos[4]),
            Equipo(id: 52, orden: 2, nombre: "Suiza", grupo: self.grupos[4]),
            Equipo(id: 53, orden: 3, nombre: "Costa Rica", grupo: self.grupos[4]),
            Equipo(id: 54, orden: 4, nombre: "Serbia", grupo: self.grupos[4]),
            //Grupo F
            Equipo(id: 61, orden: 1, nombre: "Alemania", grupo: self.grupos[5]),
            Equipo(id: 62, orden: 2, nombre: "México", grupo: self.grupos[5]),
            Equipo(id: 63, orden: 3, nombre: "Suecia", grupo: self.grupos[5]),
            Equipo(id: 64, orden: 4, nombre: "Corea del Sur", grupo: self.grupos[5]),
            //Grupo G
            Equipo(id: 71, orden: 1, nombre: "Bélgica", grupo: self.grupos[6]),
            Equipo(id: 72, orden: 2, nombre: "Panamá", grupo: self.grupos[6]),
            Equipo(id: 73, orden: 3, nombre: "Túnez", grupo: self.grupos[6]),
            Equipo(id: 74, orden: 4, nombre: "Inglaterra", grupo: self.grupos[6]),
            //Grupo H
            Equipo(id: 81, orden: 1, nombre: "Polonia", grupo: self.grupos[7]),
            Equipo(id: 82, orden: 2, nombre: "Senegal", grupo: self.grupos[7]),
            Equipo(id: 83, orden: 3, nombre: "Colombia", grupo: self.grupos[7]),
            Equipo(id: 84, orden: 4, nombre: "Japón", grupo: self.grupos[7]),
        ]
        
        self.grupos[0].equipos = [
            self.equipos[0],
            self.equipos[1],
            self.equipos[2],
            self.equipos[3]
        ]
        
        self.grupos[1].equipos = [
            self.equipos[4],
            self.equipos[5],
            self.equipos[6],
            self.equipos[7]
        ]
        
        self.grupos[2].equipos = [
            self.equipos[8],
            self.equipos[9],
            self.equipos[10],
            self.equipos[11]
        ]
        
        self.grupos[3].equipos = [
            self.equipos[12],
            self.equipos[13],
            self.equipos[14],
            self.equipos[15]
        ]
        
        self.grupos[4].equipos = [
            self.equipos[16],
            self.equipos[17],
            self.equipos[18],
            self.equipos[19]
        ]
        
        self.grupos[5].equipos = [
            self.equipos[20],
            self.equipos[21],
            self.equipos[22],
            self.equipos[23]
        ]
        
        self.grupos[6].equipos = [
            self.equipos[24],
            self.equipos[25],
            self.equipos[26],
            self.equipos[27]
        ]
        
        self.grupos[7].equipos = [
            self.equipos[28],
            self.equipos[29],
            self.equipos[30],
            self.equipos[31]
        ]
        
        self.partidos = [
            Partido(id: 1, fecha: "2018-06-14 18:00", equipoA: self.equipos[1], equipoB: self.equipos[2], resultadoA: nil, resultadoB: nil),
            Partido(id: 2, fecha: "2018-06-15 17:00", equipoA: self.equipos[3], equipoB: self.equipos[0], resultadoA: nil, resultadoB: nil),
            Partido(id: 3, fecha: "2018-06-15 18:00", equipoA: self.equipos[7], equipoB: self.equipos[6], resultadoA: nil, resultadoB: nil),
            Partido(id: 4, fecha: "2018-06-15 21:00", equipoA: self.equipos[5], equipoB: self.equipos[4], resultadoA: nil, resultadoB: nil)
        ]
        
        self.resultados = [
            Resultado(
                id: 1,
                resultadoA: 1,
                resultadoB: 1,
                usuario: usuarios[0],
                partido: partidos[0]
            )
        ]
        
        self.apuestas = [
            Apuesta(
                id: 1,
                fecha: "Lunes, 16 de Julio 13:00",
                premio: self.premios[0],
                resultado: self.resultados[0],
                retador: self.usuarios[0],
                apostadores: self.usuarios
            )
        ]
    }
    
    //Usuarios
    
    func getUsuario(id: Int) -> Usuario? {
        return self.usuarios.first(where: {
            $0.id == id
        })
    }
    
    //Apuestas
    
    func getApuestas(retadorId: Int) -> [Apuesta] {
        return self.apuestas.filter({
            $0.retador.id == retadorId
        })
    }
    
    func insertApuesta(apuesta: Apuesta) {
        self.apuestas.append(apuesta)
    }
    
    func updateApuesta(apuesta: Apuesta) {
        let indice = self.apuestas.index(where: {$0.id == apuesta.id})
        self.apuestas[indice!] = apuesta
    }
    
    func deteteApuesta(id: Int) {
        let indice = self.apuestas.index(where: {$0.id == id})
        self.apuestas.remove(at: indice!)
    }
    
    //Premios
    
    func getPremios() -> [Premio] {
        return self.premios
    }
    
    func getPartidos() -> [Partido] {
        return self.partidos
    }
}

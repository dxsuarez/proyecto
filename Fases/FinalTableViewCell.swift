//
//  FinalTableViewCell.swift
//  proyecto
//
//  Created by Wilson Gabriel Ramos Bravo on 9/8/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class FinalTableViewCell: UITableViewCell {

    @IBOutlet var equipoLocalLabel: UILabel!
    @IBOutlet var equipoVisitanteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

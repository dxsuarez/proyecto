struct Partido {
    var id: Int
    var fecha: String
    var equipoA: Equipo
    var equipoB: Equipo
    var resultadoA: Int?
    var resultadoB: Int?
}

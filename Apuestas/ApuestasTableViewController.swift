//
//  ApuestasTableViewController.swift
//  proyecto
//
//  Created by Diego Suárez on 31/7/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class ApuestasTableViewController: UITableViewController {
    @IBOutlet var apuestasTableView: UITableView!
    
    //var data = Data()
    let usuarioId = 1
    var apuestas: [Apuesta] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        apuestas = data.apuestas//data.getApuestas(retadorId: usuarioId)
        print("Apuestas")
        print("\(data.apuestas.count)")
        
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return apuestas.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        /*
        //let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        // Configure the cell...
        let cell:UITableViewCell=UITableViewCell(style: UITableViewCellStyle.value2, reuseIdentifier: "mycell")
        let grupo = data.grupos[indexPath.row]
        //tableView.rowHieght = 100
        cell.textLabel?.text  = "id:\(grupo.id), name:\(grupo.nombre), orden:\(grupo.orden)"
        //cell.imageView!.image = UIImage(named: "\(grupo.id)")!
        cell.detailTextLabel?.text = "este es el subtitulo"
        cell.backgroundColor = UIColor.blue
        cell.textLabel?.textColor = UIColor.brown
        //cell.textLabel?.font = UIFont.systemFontSize()
        cell.accessoryType = UITableViewCellAccessoryType.checkmark
        
        return cell
        */
        /*
        let cell = UITableViewCell()
        let grupo = data.grupos[indexPath.row]
        let text = "id:\(grupo.id), name:\(grupo.nombre), orden:\(grupo.orden)"
        cell.textLabel?.text = text
        cell.detailTextLabel?.text = "detalle"
        
        return cell
 */
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApuestasCell") as! ApuestasTableViewCell
        let apuesta = apuestas[indexPath.row]
        
        cell.amigosLabel?.text = "Lunes, 16 de junio 13:00, \(String(describing: apuesta.apostadores?.count)) amigos"
        cell.premioImageView!.image = UIImage(named: "p-\(apuesta.premio.id)")!
        cell.resultadoLabel?.text = "\(apuesta.resultado.resultadoA) - \(apuesta.resultado.resultadoB)"
        cell.partidoLabel?.text = apuesta.resultado.partido.fecha
        
        return cell
        
    }
    
    // Mark Unwind Segues
    @IBAction func unwindFromDetalleApuestas(_ segue:UIStoryboardSegue) {
        apuestas = data.apuestas
        apuestasTableView.reloadData()
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
 
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100;
    }
*/
    
}

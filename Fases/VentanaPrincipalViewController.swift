import UIKit


class VentanaPrincipalViewController: UIViewController {

    //MARK:-Outlet
    //MARK:-Load
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:-Action
    
    @IBAction func FaseGruposButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceFases", sender: self)
    }
    
    @IBAction func OctavosFinalButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlacesOctavos", sender: self)
    }
    
    @IBAction func CuartosFinalButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceCuartos", sender: self)
    }
    
    @IBAction func SemifinalButton(_ sender: Any) {
        performSegue(withIdentifier: "EnlaceSemifinal", sender: self)
    }
    
    @IBAction func FinalButton(_ sender: Any) {
        performSegue(withIdentifier: "FinalSegue", sender: self)
    }
}

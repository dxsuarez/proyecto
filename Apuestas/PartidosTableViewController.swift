//
//  PartidosTableViewController.swift
//  proyecto
//
//  Created by Desarrollo on 8/11/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class PartidosTableViewController: UITableViewController {

    var data = Data()
    var partidos: [Partido] = []
    var partido: Partido?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        partidos = data.getPartidos()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return partidos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PartidosCell") as! PartidosTableViewCell
        let partido = partidos[indexPath.row]
        
        cell.fechaPartidoLabel?.text = partido.fecha
        cell.equipoAImageView!.image = UIImage(named: "e-\(partido.equipoA.id)")!
        cell.equipoBImageView!.image = UIImage(named: "e-\(partido.equipoB.id)")!
        cell.resultadoLabel?.text = "\(String(describing: partido.resultadoA)) - \(String(describing: partido.resultadoB))"
        cell.equipoALabel?.text = partido.equipoA.nombre
        cell.equipoBLabel?.text = partido.equipoB.nombre
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let usuario = data.getUsuario(id: 1)
        let resultado = Resultado(
            id: 0,
            resultadoA: 0,
            resultadoB: 0,
            usuario: usuario!,
            partido: partido!
        )
    
        let destination = segue.destination as! ResultadoViewController
        destination.resultado = resultado
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        partido = partidos[indexPath.row]
        performSegue(withIdentifier: "toResultadoSegue", sender: self)
    }

}

//
//  DetalleApuestaTableViewController.swift
//  proyecto
//
//  Created by Diego Suárez on 9/8/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class ApuestaTableViewController: UITableViewController {

  
    @IBOutlet weak var amigosLabel: UILabel!
    @IBOutlet weak var fechaApuesta: UILabel!
    @IBOutlet weak var premioImageView: UIImageView!
    @IBOutlet weak var equipoAImageView: UIImageView!
    @IBOutlet weak var equipoBImageView: UIImageView!
    @IBOutlet weak var equipoALabel: UILabel!
    @IBOutlet weak var equipoBLabel: UILabel!
    @IBOutlet weak var fechaPartidoLabel: UILabel!
    @IBOutlet weak var resultadoLabel: UILabel!
    
    //var data = Data()
    let usuarioId = 1
    var premio: Premio?
    var resultado: Resultado?
    var apostadores: [Usuario] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //fechaApuesta.text = "hoy";
        apostadores = data.gruposAmigos[0].amigos
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell=UITableViewCell()
        
        let section = indexPath.section
        
        if(section == 0 && indexPath.row == 1) {
            let cell3 = tableView.de.dequeueReusableCell(withIdentifier: "PremioCell") as? UITableViewCell
            return cell3!
     
        }
        
        
        if(section != 2 || (section == 2 && indexPath.row == 0)) {
            return cell
        }
        
        var cell2:UITableViewCell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "mycell")
        
        let amigo = apostadores[indexPath.row]
        cell2.textLabel?.text  = "\(amigo.nombre)"
        cell2.imageView!.image = UIImage(named: "u-\(amigo.id)")!
        cell2.detailTextLabel?.text = "este es el subtitulo"
        cell2.accessoryType = .none
        
        return cell2
    }
 */
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let section = indexPath.section
        var segueId = ""
        
        switch section {
        case 0 :
            segueId = "toPremiosSegue"
            break
        case 1 :
            segueId = "toPartidosSegue"
            break
        case 2 :
            segueId = "toAmigosSegue"
            break
        default:
            segueId = ""
        }
        
        performSegue(withIdentifier: segueId, sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
    
    @IBAction func unwindFromPremios(_ segue:UIStoryboardSegue) {
        if let source = segue.source as? PremiosCollectionViewController,
            let premioSeleccionado = source.premio {
            premio = premioSeleccionado
        }
        
        
        let premioId = premio?.id
        
        
        fechaApuesta.text = premio?.nombre
        premioImageView.image = UIImage(named: "p-\(premioId ?? 1)")!
        //premioImageView.image = UIImage(named: "p-1")!
    }
    
    @IBAction func unwindFromAmigos(_ segue:UIStoryboardSegue) {
        if let source = segue.source as? AmigosTableViewController,
            let amigosSeleccionados = source.apostadores {
            apostadores = amigosSeleccionados
        }
        
        
        
        let cantidad = apostadores.count
        
        
        
        amigosLabel.text = "Invitaste a \(cantidad) amigos"
    }
    
    @IBAction func aceptarButtonPressed(_ sender: Any) {
        let usuario = data.getUsuario(id: 1)
        let apuesta = Apuesta(
            id: 1,
            fecha: "Lunes, 16 de Julio 13:00",
            premio: premio!,
            resultado: resultado!,
            retador: usuario!,
            apostadores: apostadores
        )
        data.insertApuesta(apuesta: apuesta)
        print("apuesta")
        print("\(data.apuestas.count)")
        performSegue(withIdentifier: "unwindFromDetalleApuestas", sender: self)
    }
    
    @IBAction func unwindFromResultado(_ segue:UIStoryboardSegue) {
        if let source = segue.source as? ResultadoViewController,
            let resultadoSeleccionado = source.resultado {
            resultado = resultadoSeleccionado
        }
        fechaApuesta.text = resultado?.partido.equipoA.nombre
        
        let partido = resultado!.partido
        
        fechaPartidoLabel.text = partido.fecha
        equipoAImageView.image = UIImage(named: "e-\(partido.equipoA.id)")!
        equipoBImageView.image = UIImage(named: "e-\(partido.equipoB.id)")!
        resultadoLabel.text = "\(resultado!.resultadoA) - \(resultado!.resultadoB)"
        equipoALabel.text = partido.equipoA.nombre
        equipoBLabel.text = partido.equipoB.nombre
        
    }
    /*
    override func viewWillDisappear(_ animated: Bool) {
        //performSegue(withIdentifier: "unwindFromAmigos", sender: self)
        
        
        super.viewWillDisappear(animated)
        
    }
 */
}

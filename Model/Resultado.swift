struct Resultado {
    var id: Int
    var resultadoA: Int
    var resultadoB: Int
    var usuario: Usuario
    var partido: Partido
}

struct Grupo {
    var id: Int
    var orden: Int
    var nombre: String
    var equipos: [Equipo]?
    
    /*
    init(id: Int, orden: Int, nombre: String, equipos: [Equipo]) {
        self.id = id
        self.orden = orden
        self.nombre = nombre
        self.equipos = equipos
    }
 */
}

struct Apuesta {
    var id: Int
    var fecha: String
    var premio: Premio
    var resultado: Resultado
    var retador: Usuario
    var apostadores: [Usuario]?
}

//
//  ResultadoViewController.swift
//  proyecto
//
//  Created by Desarrollo on 8/12/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class ResultadoViewController: UIViewController {

    @IBOutlet weak var resultadoALabel: UILabel!
    @IBOutlet weak var resultadoBLabel: UILabel!
    @IBOutlet weak var equipoAImageView: UIImageView!
    @IBOutlet weak var equipoBImageView: UIImageView!
    @IBOutlet weak var equipoALabel: UILabel!
    @IBOutlet weak var equipoBLabel: UILabel!
    @IBOutlet weak var fechaPartidoLabel: UILabel!
    
    var resultado: Resultado?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let partido = resultado!.partido
        
        fechaPartidoLabel.text = partido.fecha
        equipoAImageView!.image = UIImage(named: "e-\(partido.equipoA.id)")!
        equipoBImageView!.image = UIImage(named: "e-\(partido.equipoB.id)")!
        resultadoALabel?.text = "0"
        resultadoBLabel?.text = "0"
        equipoALabel?.text = partido.equipoA.nombre
        equipoBLabel?.text = partido.equipoB.nombre
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func resultadoAStepperValueChanged(_ sender: UIStepper) {
        let valor = Int(sender.value)
        resultado?.resultadoA = valor
        resultadoALabel.text = "\(valor)"
    }
    
    
    @IBAction func resultadoBStepperValueChanged(_ sender: UIStepper) {
        let valor = Int(sender.value)
        resultado?.resultadoB = valor
        resultadoBLabel.text = "\(valor)"
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

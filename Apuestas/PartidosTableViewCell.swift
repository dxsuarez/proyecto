//
//  ApuestasTableViewCell.swift
//  proyecto
//
//  Created by Diego Suárez on 1/8/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class PartidosTableViewCell: UITableViewCell {

    @IBOutlet weak var fechaPartidoLabel: UILabel!
    @IBOutlet weak var equipoAImageView: UIImageView!
    @IBOutlet weak var equipoBImageView: UIImageView!
    @IBOutlet weak var resultadoLabel: UILabel!
    @IBOutlet weak var equipoALabel: UILabel!
    @IBOutlet weak var equipoBLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

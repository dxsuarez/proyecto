//
//  ApuestasTableViewCell.swift
//  proyecto
//
//  Created by Desarrollo on 8/11/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit

class ApuestasTableViewCell: UITableViewCell {

    @IBOutlet weak var amigosLabel: UILabel!
    @IBOutlet weak var premioImageView: UIImageView!
    @IBOutlet weak var resultadoLabel: UILabel!
    @IBOutlet weak var partidoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

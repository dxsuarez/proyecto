//
//  CuartosViewController.swift
//  proyecto
//
//  Created by Wilson Gabriel Ramos Bravo on 8/9/18.
//  Copyright © 2018 Proyecto iOS. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class CuartosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var jsonArray: NSArray?
    var equipoLocal: Array<String> = []
    var equipoVisitante: Array<String> = []
    
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return equipoLocal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell1 = tableView.dequeueReusableCell(withIdentifier: "CuartosCell", for: indexPath) as! CuartosTableViewCell
        cell1.equipoLocalLabel.text = self.equipoLocal[indexPath.row]
        cell1.equipoVisitanteLabel.text = self.equipoVisitante[indexPath.row]
        return cell1
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        llenadoTabla()
        tableView.reloadData()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func llenadoTabla(){
        
        Alamofire.request("https://apifootball.com/api/?action=get_events&from=2018-06-30&to=2018-07-15&league_id=1802&APIkey=cb0c5e4d210bb0390a203e80fdf8739f4afb7e96adb56c0f743b38a27ca64cb1") .responseJSON { response in
            if let JSON = response.value{
                self.jsonArray = JSON as? NSArray
                for item in self.jsonArray! as! [NSDictionary]{
                    
                    let nameLocal = item["match_hometeam_name"] as? String
                    let nameVisitante = item["match_awayteam_name"] as? String
                    self.equipoLocal.append((nameLocal)!)
                    self.equipoVisitante.append((nameVisitante)!)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "CUARTOS DE FINAL"
        default :
            return "CUARTOS DE FINAL"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1 {
            performSegue(withIdentifier: "cuartos1", sender: self)
        }
        else if indexPath.row == 4 {
            performSegue(withIdentifier: "cuartos2", sender: self)
        }
        else if indexPath.row == 2 {
            performSegue(withIdentifier: "cuartos3", sender: self)
        }
        else {
            performSegue(withIdentifier: "cuartos4", sender: self)
        }
    }


}
